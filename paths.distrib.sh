export didfail=~/didfail
export wkspc=~/workspaces/didfail
export sdk_platforms=$didfail/platforms

# The max_mem variable is fed to "ulimit -v".
# The "-Xmx" option in jvm_flags specifies max Java heap size.
# If the Java VM dies with an "insufficient memory" error, 
# then raise max_mem or lower the heap size ("-Xmx" option).
export jvm_flags="-Xmx950m -Xss32m"
export max_mem=2250000
export max_time=600

export python=python2
export rt_jar=/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar
export android_jar=$sdk_platforms/android-16/android.jar

export newsoot=1
export soot_base=$wkspc
export epicc_dir=$didfail/epicc
export dare_dir=$didfail/dare-1.1.0-linux
export dare=$dare_dir/dare
export cert_apk_transform_dir=$didfail/cert/transformApk


################################################################################
# Nothing below this line needs to be modified.
################################################################################

sp=
sp=$sp:$soot_base/soot/classes
sp=$sp:$soot_base/soot/libs/polyglot.jar
sp=$sp:$soot_base/soot/libs/AXMLPrinter2.jar
sp=$sp:$soot_base/jasmin/classes
sp=$sp:$soot_base/jasmin/libs/java_cup.jar
sp=$sp:$soot_base/heros/bin
sp=$sp:$soot_base/heros/guava-14.0.1.jar
sp=$sp:$soot_base/heros/slf4j-api-1.7.5.jar
sp=$sp:$soot_base/heros/slf4j-simple-1.7.5.jar

if [ $newsoot -eq 1 ]; then
    sp=$sp:$soot_base/soot/libs/hamcrest-all-1.3.jar
    sp=$sp:$soot_base/soot/libs/junit-4.11.jar
    sp=$sp:$soot_base/soot/libs/dexlib2-2.0.3-dev.jar
    sp=$sp:$soot_base/soot/libs/util-2.0.3-dev.jar
    sp=$sp:$soot_base/soot/libs/asm-debug-all-5.0.3.jar
    sp=$sp:$soot_base/heros/junit.jar
    sp=$sp:$soot_base/heros/org.hamcrest.core_1.3.0.jar
else 
    sp=$sp:$soot_base/soot/libs/baksmali-2.0b5.jar
    sp=$sp:$soot_base/soot/libs/baksmali-1.3.2.jar
fi
export soot_paths=$sp

testpaths=$cert_apk_transform_dir/bin:$soot_paths:$android_jar:$rt_jar
testpaths=$testpaths:$epicc_dir:$dare:$sdk_platforms:$wkspc:$soot_base

# Verify that paths don't contain spaces
spaced_paths=`echo $testpaths | egrep -o "[^:]* [^:]*"`
if [ "$spaced_paths" != "" ]; then
    echo "Error: some paths contains spaces!"
    echo $testpaths | egrep -o "[^:]* [^:]*"
fi

# Check existence of jars and directories:
ls -d $(echo $testpaths | tr ':' ' ') > /dev/null

